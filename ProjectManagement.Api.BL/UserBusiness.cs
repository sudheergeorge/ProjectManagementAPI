﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectManagement.DAL;
using ProjectManagement.Entities;

namespace ProjectManagement.BL
{   

    /// <summary>
    /// Encapsulates all operations to manage Users
    /// </summary>
    public class UserBusiness
    {
        #region Private Fileds
        private readonly UserRepository repoUser;
        #endregion

        #region Constructor
        public UserBusiness()
        {
            repoUser = new UserRepository();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Returns all users
        /// </summary>
        /// <returns></returns>
        public List<UserModel> GetAllUsers()
        {
            return repoUser.GetAllUsers().Select(x => new UserModel
            {
                Employee_ID = x.Employee_ID,
                First_Name = x.First_Name,
                Last_Name = x.Last_Name,
                Project_ID = x.Project_ID,
                User_ID = x.User_ID
            }).ToList();
        }

        /// <summary>
        /// Updates a User's detail
        /// </summary>
        /// <param name="oUser"></param>
        /// <returns></returns>
        public UserUpdateResult UpdateUser(UserModel oUser)
        {
            Status oStatus = new Status();
            User user = new User()
            {
                Employee_ID = oUser.Employee_ID,
                First_Name = oUser.First_Name,
                Last_Name = oUser.Last_Name,
                Project_ID = oUser.Project_ID
            };
            if (oUser.User_ID == 0)
            {
                user = repoUser.AddUser(user);
                oStatus = new Status() { Message = "User added successfully", Result = true };
            }
            else
            {
                user.User_ID = oUser.User_ID;
                user = repoUser.UpdateUser(user);
                oStatus = new Status() { Message = "User updated successfully", Result = true };
            }

            return new UserUpdateResult()
            {
                status = oStatus,
                user = new UserModel()
                {
                    User_ID = user.User_ID,
                    Project_ID = user.Project_ID,
                    Employee_ID = user.Employee_ID,
                    First_Name = user.First_Name,
                    Last_Name = user.Last_Name
                }
            };

        }

        /// <summary>
        /// Deletes a user
        /// </summary>
        /// <param name="oUser"></param>
        /// <returns></returns>
        public Status DeleteUser(UserModel oUser)
        {
            repoUser.DeleteUser(new User()
            {
                User_ID = oUser.User_ID
            });
            return new Status() { Message = "user deleted successfully", Result = true };
        }

        #endregion
    }
}
