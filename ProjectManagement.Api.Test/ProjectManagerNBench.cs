﻿using NBench;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectManagmentApi.Controllers;

namespace ProjectManagementApi.Test
{
    class ProjectManagerNBench
    {
        private const string AddCounterName = "AddCounter";
        private Counter addCounter;
        private int key;

        private const int AcceptableMinAddThroughput = 100;

        [PerfSetup]
        public void Setup(BenchmarkContext context)
        {
            addCounter = context.GetCounter(AddCounterName);
            key = 0;
        }
        [PerfBenchmark(NumberOfIterations = 500, RunMode = RunMode.Throughput, RunTimeMilliseconds = 600000, TestMode = TestMode.Measurement)]
        [CounterMeasurement(AddCounterName)]
        [CounterThroughputAssertion(AddCounterName, MustBe.GreaterThan, AcceptableMinAddThroughput)]
        public void GetAllUsersCounterThroughputBenchMark(BenchmarkContext context)
        {
            UserController oController = new UserController();
            oController.Get();
            addCounter.Increment();
        }
    }
}
