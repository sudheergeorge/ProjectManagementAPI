namespace ProjectManagement.Entities
{
    /// <summary>
    /// Represents a User entity
    /// </summary>
    public partial class UserModel
    {
        /// <summary>
        /// Gets/sets user id
        /// </summary>
        public long User_ID { get; set; }

       /// <summary>
       /// Gets/set first name
       /// </summary>
        public string First_Name { get; set; }

       /// <summary>
       /// Gets/sets last name
       /// </summary>
        public string Last_Name { get; set; }

        /// <summary>
        /// Gets/sets employee id
        /// </summary>
        public string Employee_ID { get; set; }

        /// <summary>
        /// gets/sets project id
        /// </summary>
        public long? Project_ID { get; set; }

    }
}
