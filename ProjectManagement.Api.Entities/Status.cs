﻿namespace ProjectManagement.Entities
{
    /// <summary>
    /// Represents Status
    /// </summary>
    public class Status
    {
        /// <summary>
        /// Status Message
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// Status Result
        /// </summary>
        public bool Result { get; set; }
    }
}
