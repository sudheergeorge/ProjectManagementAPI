﻿namespace ProjectManagement.Entities
{
    /// <summary>
    /// Represnts status of a user Update operation
    /// </summary>
    public class UserUpdateResult
    {
        /// <summary>
        /// Update status
        /// </summary>
        public Status status { get; set; }

        /// <summary>
        /// User that is updated
        /// </summary>
        public UserModel user { get; set; }
    }
}
