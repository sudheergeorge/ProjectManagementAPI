﻿
namespace ProjectManagement.Entities
{
    /// <summary>
    /// Represnts status of a task Update operation
    /// </summary>
    public class TaskUpdateResult
    {
        /// <summary>
        /// Update status
        /// </summary>
        public Status status { get; set; }

        /// <summary>
        /// Task that is updated
        /// </summary>
        public TaskModel task { get; set; }
    }
}
