﻿namespace ProjectManagement.Entities
{
    /// <summary>
    /// Represnts status of a project Update operation
    /// </summary>
    public class ProjectUpdateResult
    {
        /// <summary>
        /// Update status
        /// </summary>
        public Status status { get; set; }

        /// <summary>
        /// Project that is updated
        /// </summary>
        public ProjectModel project { get; set; }
    }
}
