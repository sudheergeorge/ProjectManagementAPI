﻿using System;

namespace ProjectManagement.Entities
{
    /// <summary>
    /// Represents a Project
    /// </summary>
    public class ProjectModel
    {
        /// <summary>
        /// Unique Identifier for the project
        /// </summary>
        public long Project_ID { get; set; }

        /// <summary>
        /// Name of the project
        /// </summary>
        public string ProjectName { get; set; }

        /// <summary>
        /// Project Start Date
        /// </summary>
        public DateTime? Start_Date { get; set; }

        /// <summary>
        /// Project End Date
        /// </summary>
        public DateTime? End_Date { get; set; }

        /// <summary>
        /// Project priority
        /// </summary>
        public short? Priority { get; set; }

        /// <summary>
        /// Project Status
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Number of tasks associated with this project
        /// </summary>
        public int? NumberOfTasks { get; set; }

        /// <summary>
        /// Project manager's ID
        /// </summary>
        public long? Manager_ID { get; set; }

        /// <summary>
        /// Name of the project manager
        /// </summary>
        public string Manager_Name { get; set; }
    }
}
