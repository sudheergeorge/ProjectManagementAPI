﻿namespace ProjectManagement.Entities
{
    /// <summary>
    /// Represents a Parent Task
    /// </summary>
    public class ParentTaskModel
    {
        /// <summary>
        /// Unique Identier for this task
        /// </summary>
        public long Parent_ID { get; set; }
        /// <summary>
        /// Name of this task
        /// </summary>
        public string Parent_Name { get; set; }

    }
}
