﻿using System;

namespace ProjectManagement.Entities
{
    /// <summary>
    /// Represents a Task 
    /// </summary>
    public class TaskModel
    {
        /// <summary>
        /// Gets/sets Task ID
        /// </summary>
        public long Task_ID { get; set; }

        /// <summary>
        /// Gets/sets parent Task ID
        /// </summary>
        public long? Parent_ID { get; set; }

        /// <summary>
        /// Gets/sets pare Task name
        /// </summary>
        public string Parent_Name { get; set; }

        /// <summary>
        /// Gets/sets project ID
        /// </summary>
        public long? Project_ID { get; set; }
        /// <summary>
        /// Gets/sets project name
        /// </summary>
        public string Project_Name { get; set; }

        /// <summary>
        /// Gets/sets Task Name
        /// </summary>
        public string TaskName { get; set; }
        /// <summary>
        /// Gets/sets Task Start date
        /// </summary>
        public DateTime? Start_Date { get; set; }

        /// <summary>
        /// Gets/sets Task End date
        /// </summary>
        public DateTime? End_Date { get; set; }

        public short? Priority { get; set; }

        public bool? Status { get; set; }

        public long? User_ID { get; set; }
        public string User_Name { get; set; }
    }
}
